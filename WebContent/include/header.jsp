<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="beermon.data.BmSessionData" %>
<%@ page import="beermon.data.BmSessionData.Message" %>
    
<div id="headerLeft">
	<a href="/"><img src="../images/beermon.png"/></a>
</div>
<div id="headerRight">
	<span>
		<%
		String user = (String)request.getSession().getAttribute("BmUserName"); 
		if(user != null) {
			out.println("<a href=\"/admin/managetaps.jsp\">Taps</a>");
			out.println("&nbsp;&nbsp;|&nbsp;&nbsp;");
			out.println("<a href=\"/admin/managebeers.jsp\">Beers</a>");
			out.println("&nbsp;&nbsp;|&nbsp;&nbsp;");
			out.println("<a href=\"/admin/manageusers.jsp\">Users</a>");
			out.println("&nbsp;&nbsp;|&nbsp;&nbsp;");
			out.println("<a href=\"/login/logout\">Log Out</a>");
			out.println("&nbsp;&nbsp;|&nbsp;&nbsp;");
			out.println("<b>" + user + "</b>");
		} else {
			out.println("&nbsp;");
		}
		%>
	</span>
</div>
<img id="disconnectImg" src="/images/alert.png"/>
<%
Message msg = BmSessionData.getMessage(request);
if(msg != null)
	out.println("<div id=\"msg\" class=\"" + (msg.msgGood ? "msgSuccess" : "msgError") + "\" onClick=\"javascript:(document.getElementById('msg').style.display = 'none');\">" + msg.msg + "</div>");
%>
<br/>
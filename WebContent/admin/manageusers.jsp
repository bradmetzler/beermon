<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="java.util.List" %>

<%@ page import="beermon.data.BmData" %>
<%@ page import="beermon.object.BmUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Beermon</title>
		<link rel="stylesheet" type="text/css" href="/beermon.css">
		<script type="text/javascript">
			function deleteUser(id)
			{
				if(confirm("Are you sure you want to delete this user?"))
				{
					window.location.href = "/admin/deleteuser?id=" + id;
				}
			}
		</script>
	</head>
	<body>
		<%@ include file="/include/header.jsp" %>
		<div id="content">
			<div id="userTable">
				<table class="dataTable">
					<tr>
						<th class="nameCol">User</th>
						<th class="delCol">&nbsp;</th>
					</tr>
					<%
					List<BmUser> bmusers = BmData.getUsers();
					if(bmusers != null) {
						for(BmUser bmuser : bmusers) {%>
							<tr>
								<td class="nameCol"><%=bmuser.name%></td>
								<td class="delCol"><a href="javascript:deleteUser(<%=bmuser.id%>);"><img src="/images/icon_delete.png"/></a></td>
							</tr>
						<%}
					}
					%>
				</table>
				<br/>
				<a href="/admin/createuser.jsp"><button type="button">New User</button></a>
			</div>
		</div>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="java.util.List" %>
<%@ page import="beermon.data.BmData" %>
<%@ page import="beermon.object.BmBeer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Beermon</title>
		<link rel="stylesheet" type="text/css" href="/beermon.css">
		<script type="text/javascript">
			function deleteBeer(id)
			{
				if(confirm("Are you sure you want to delete this beer?"))
				{
					window.location.href = "/admin/deletebeer?id=" + id;
				}
			}
		</script>
	</head>
	<body>
		<%@ include file="/include/header.jsp" %>
		<div id="content">
			<div id="beerTable">
				<table class="dataTable">
					<tr>
						<th class="nameCol">Beer Name</th>
						<th class="styleCol">Style</th>
						<th class="locationCol">Location</th>
						<th class="abvCol">ABV</th>
						<th class="ibuCol">IBU</th>
						<th class="imageCol">Image</th>
						<th class="editCol">&nbsp;</th>
						<th class="delCol">&nbsp;</th>
					</tr>
					<%
					List<BmBeer> beers = BmData.getBeers();
					if(beers != null && !beers.isEmpty()) {
						for(BmBeer beer : beers) {%>
							<tr>
								<td class="nameCol"><%=beer.name%></td>
								<td class="styleCol"><%=beer.style%></td>
								<td class="locationCol"><%=beer.location%></td>
								<td class="abvCol"><%=beer.abv != null && !beer.abv.isEmpty() ? beer.abv + "%" : ""%></td>
								<td class="ibuCol"><%=beer.ibu%></td>
								<%if(beer.hasImage()){%>
								<td class="imageCol"><a href="/beer/image/<%=beer.id%>">Image</a></td>
								<%} else {%>
								<td class="imageCol">&nbsp;</td>
								<%}%>
								<td class="editCol"><a href="/admin/createbeer.jsp?editBeer=<%=beer.id%>"><img src="/images/icon_edit.png"/></a></td>
								<td class="delCol"><a href="javascript:deleteBeer(<%=beer.id%>);"><img src="/images/icon_delete.png"/></a></td>
							</tr>
						<%}
					} else {%>
							<tr>
								<td colspan="8" style="text-align:center;"><i>(No Beers)</i></td>
							</tr>
					<%}%>
				</table>
				<br/>
				<a href="/admin/createbeer.jsp"><button type="button">New Beer</button></a>
			</div>
		</div>
	</body>
</html>
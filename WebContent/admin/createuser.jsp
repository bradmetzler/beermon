<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Beermon</title>
		<link rel="stylesheet" type="text/css" href="/beermon.css">
	</head>
	<body>
		<%@ include file="/include/header.jsp" %>
		<div id="content">
			<div id="userForm">
				<h2>New User</h2>
				<form method="POST" action="/admin/createuser">
					<table border="0">
						<tr>
							<td>User:</td>
							<td><input type="text" name="user" autofocus required/></td>
						</tr>
						<tr>
							<td>Password:</td>
							<td><input type="password" name="pass" required/></td>
						</tr>
						<tr>
							<td>Confirm:</td>
							<td><input type="password" name="pass2" required/></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" value="Create User"/></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="java.util.List" %>
<%@ page import="beermon.data.BmData" %>
<%@ page import="beermon.object.BmBeer" %>
<%@ page import="beermon.object.BmSensor" %>

<%
List<BmBeer> beers = BmData.getBeers();
BmBeer onKcTapLeft = BmData.getBeerOnTap("KC Left Tap");
BmBeer onKcTapRight = BmData.getBeerOnTap("KC Right Tap");
BmBeer onDallasTapLeft = BmData.getBeerOnTap("Dallas Left Tap");
BmBeer onDallasTapRight = BmData.getBeerOnTap("Dallas Right Tap");
BmSensor kcKegTemp = BmData.getSensor("KC Keg Temperature");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Beermon</title>
		<link rel="stylesheet" type="text/css" href="/beermon.css">
	</head>
	<body>
		<%@ include file="/include/header.jsp" %>
		<div id="content">
			<div id="tapTable">
				<form method="POST" action="/admin/updateTaps">
					<h2>On Tap in Kansas City</h2>
					<table class="dataTable">
						<tr>
							<th class="tapCol">Tap</th>
							<th class="beerCol">Beer</th>
						</tr>
						<tr>
							<td>KC Left Tap:</td>
							<td>
								<select name="kcLeftTapBeerId">
									<option value="-1">(Empty)</option>
									<%for(BmBeer beer : beers){%>
									<option value="<%=beer.id%>"<%=onKcTapLeft != null && onKcTapLeft.id == beer.id ? " selected" : ""%>><%=beer.name%></option>
									<%}%>
								</select>
							</td>
						</tr>
						<tr>
							<td>KC Right Tap:</td>
							<td>
								<select name="kcRightTapBeerId">
									<option value="-1">(Empty)</option>
									<%for(BmBeer beer : beers){%>
									<option value="<%=beer.id%>"<%=onKcTapRight != null && onKcTapRight.id == beer.id ? " selected" : ""%>><%=beer.name%></option>
									<%}%>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%if(kcKegTemp.hasData()) {%>
								KC keg temperature is <%=kcKegTemp.getValueWithUnit()%> as of <%=kcKegTemp.getFormattedLastUpdateTime()%>.
								<%} else {%>
								KC keg temperature is unknown!
								<%}%>
							</td>						
						</tr>
					</table>
					<br/>
					<h2>On Tap in Dallas</h2>
					<table class="dataTable">
						<tr>
							<th class="tapCol">Tap</th>
							<th class="beerCol">Beer</th>
						</tr>
						<tr>
							<td>Dallas Left Tap:</td>
							<td>
								<select name="dallasLeftTapBeerId">
									<option value="-1">(Empty)</option>
									<%for(BmBeer beer : beers){%>
									<option value="<%=beer.id%>"<%=onDallasTapLeft != null && onDallasTapLeft.id == beer.id ? " selected" : ""%>><%=beer.name%></option>
									<%}%>
								</select>
							</td>
						</tr>
						<tr>
							<td>Dallas Right Tap:</td>
							<td>
								<select name="dallasRightTapBeerId">
									<option value="-1">(Empty)</option>
									<%for(BmBeer beer : beers){%>
									<option value="<%=beer.id%>"<%=onDallasTapRight != null && onDallasTapRight.id == beer.id ? " selected" : ""%>><%=beer.name%></option>
									<%}%>
								</select>
							</td>
						</tr>
					</table>
					<br/>
					<input type="submit" value="Save"/>
				</form>
			</div>
		</div>
	</body>
</html>
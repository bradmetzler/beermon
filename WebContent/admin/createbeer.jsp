<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="beermon.data.BmData" %>
<%@ page import="beermon.data.BmSessionData" %>
<%@ page import="beermon.object.BmBeer" %>

<%
boolean editing = false;
BmBeer editBeer = BmSessionData.getBeer(request);
if(editBeer == null && request.getParameter("editBeer") != null) {
	try {
		int editBeerId = Integer.parseInt(request.getParameter("editBeer"));
		editBeer = BmData.getBeer(editBeerId);
	} catch(Exception e) {}
}

int idVal = -1;
String nameVal = "";
String styleVal = "";
String abvVal = "";
String ibuVal = "";
String locationVal = "";
String descriptionVal = "";
boolean hasImage = false;

if(editBeer != null) {
	idVal = editBeer.id;
	nameVal = editBeer.name;
	styleVal = editBeer.style;
	abvVal = editBeer.abv;
	ibuVal = editBeer.ibu;
	locationVal = editBeer.location;
	descriptionVal = editBeer.description;
	hasImage = editBeer.hasImage();
	
	if(idVal > 0)
		editing = true;
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Beermon</title>
		<link rel="stylesheet" type="text/css" href="/beermon.css">
		<script type="text/javascript">
		function toggleExistingImage(event) {
			imgDiv = document.getElementById("editBeerExistingImage");
			if(imgDiv.style.display == "none") {
				imgDiv.style.top = event.clientY + "px";
				imgDiv.style.left = event.clientX + "px";
				imgDiv.style.display = "block";
				setTimeout(function(){document.body.onclick = function(){toggleExistingImage(null)}}, 100);
			} else {
				imgDiv.style.display = "none";
				document.body.onclick = null;
			}
		}
		function hideExistingImage() {
			document.getElementById("editBeerExistingImage").style.display = "none";
		}
		</script>
	</head>
	<body>
		<%@ include file="/include/header.jsp" %>
		<%if(editing && hasImage){%>
		<div id="editBeerExistingImage" onclick="javascript:toggleExistingImage(event);" style="display: none;"><img src="/beer/image/<%=idVal%>"/></div>
		<%}%>
		<div id="content">
			<div id="beerForm">
				<h2><%=editing ? "Edit" : "New"%> Beer</h2>
				<form method="POST" action="/admin/createbeer" enctype="multipart/form-data">
					<%if(editing){%>
					<input type="hidden" name="id" value="<%=editBeer.id%>"/>
					<%}%>
					<table border="0">
						<tr>
							<td>Name:</td>
							<td><input type="text" name="name" value="<%=nameVal%>" autofocus required/></td>
						</tr>
						<tr>
							<td>Image:</td>
							<td><input type="file" name="image"/></td>
						</tr>
						<%if(editing){%>
						<tr>
							<td>&nbsp;</td>
							<%if(hasImage){%>
							<td id="editBeerExistingImageLink"><a href="#" onclick="javascript:toggleExistingImage(event);">(Existing Image)</a></td>
							<%}else{%>
							<td id="editBeerExistingImageLink">(No Existing Image)</td>
							<%}%>
						</tr>
						<%}%>
						<tr>
							<td>Style:</td>
							<td><input type="text" name="style" value="<%=styleVal%>"/></td>
						</tr>
						<tr>
							<td>ABV:</td>
							<td><input type="number" name="abv" step="any" min="0" value="<%=abvVal%>"/></td>
						</tr>
						<tr>
							<td>IBU:</td>
							<td><input type="number" name="ibu" step="1" min="0" value="<%=ibuVal%>"/></td>
						</tr>
						<tr>
							<td>Location:</td>
							<td><input type="text" name="location" value="<%=locationVal%>"/></td>
						</tr>
						<tr>
							<td>Description:</td>
							<td><textarea name="description"><%=descriptionVal%></textarea></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" value="<%=editing ? "Save" : "Create"%> Beer"/></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body>
</html>
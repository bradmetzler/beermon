<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="beermon.data.BmData" %>
<%@ page import="beermon.object.BmBeer" %>
<%@ page import="beermon.object.BmSensor" %>

<%
long timestamp = BmData.getBeerTapTimestamp();
BmBeer onTapLeft = BmData.getBeerOnTap("KC Left Tap");
BmBeer onTapRight = BmData.getBeerOnTap("KC Right Tap");
BmSensor kcKegTemp = BmData.getSensor("KC Keg Temperature");

String leftName = "Empty";
String leftImage = "/images/beer_empty.png";
String leftStyle = "Sad Panda";
String leftLocation = "&nbsp;";
String leftIbu = "&nbsp;";
String leftAbv = "&nbsp;";
String leftDescription = "&nbsp;";
boolean leftHighAbv = false;

String rightName = "Empty";
String rightImage = "/images/beer_empty.png";
String rightStyle = "Sad Panda";
String rightLocation = "&nbsp;";
String rightIbu = "&nbsp;";
String rightAbv = "&nbsp;";
String rightDescription = "&nbsp;";
boolean rightHighAbv = false;

if(onTapLeft != null) {
	leftName = onTapLeft.name;
	leftImage = "/beer/image/" + onTapLeft.id;
	leftStyle = onTapLeft.style != null && !onTapLeft.style.isEmpty() ? onTapLeft.style : "";
	if(onTapLeft.location != null && !onTapLeft.location.isEmpty())
		leftLocation = onTapLeft.location;
	if(onTapLeft.ibu != null && !onTapLeft.ibu.isEmpty())
		leftIbu = onTapLeft.ibu + " IBU";
	if(onTapLeft.abv != null && !onTapLeft.abv.isEmpty()) {
		leftAbv = onTapLeft.abv + "% ABV";
		leftHighAbv = onTapLeft.isHighAbv();
	}
	if(onTapLeft.description != null && !onTapLeft.description.isEmpty())
		leftDescription = onTapLeft.description;
}

if(onTapRight != null) {
	rightName = onTapRight.name;
	rightImage = "/beer/image/" + onTapRight.id;
	rightStyle = onTapRight.style != null && !onTapRight.style.isEmpty() ? onTapRight.style : "";
	if(onTapRight.location != null && !onTapRight.location.isEmpty())
		rightLocation = onTapRight.location;
	if(onTapRight.ibu != null && !onTapRight.ibu.isEmpty())
		rightIbu = onTapRight.ibu + " IBU";
	if(onTapRight.abv != null && !onTapRight.abv.isEmpty()) {
		rightAbv = onTapRight.abv + "% ABV";
		rightHighAbv = onTapRight.isHighAbv();
	}
	if(onTapRight.description != null && !onTapRight.description.isEmpty())
		rightDescription = onTapRight.description;
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Beermon</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width">
		<meta name="theme-color" content="#ffffff">		
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<link rel="stylesheet" type="text/css" href="/swiper.min.css">
		<link rel="stylesheet" type="text/css" href="/beermon.css">
		<script src="/swiper.min.js"></script>
		<script src="/reconnecting-websocket.min.js"></script>
		<script type="text/javascript">
			var timestamp = <%=timestamp%>;
			function checkTimestamp() {
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
					if (this.readyState == 4) {
						if(this.status == 200) {
							var newTimestamp = this.responseText;
							if(newTimestamp > timestamp) {
								document.location.reload();
							} else {
								document.getElementById('disconnectImg').style.display = 'none';
							}
						} else {
							document.getElementById('disconnectImg').style.display = 'block';
						}
					}
				};
				xhttp.open("GET", "/taps/timestamp", true);
				xhttp.send();
			}
			
			if("WebSocket" in window) {
				var url = "ws://" + window.location.hostname + (window.location.port ? (":" + window.location.port) : "") + "/updater";
				var socket = new ReconnectingWebSocket(url);
				socket.onmessage = function(event) {
					if("Ahoy" === event.data) {
						document.location.reload();
					}
				};
			}
			
			//Websockets on the tablet are unreliable after long periods of time. Still doing the AJAX check-ins as a fallback.
			setInterval(checkTimestamp, 60000);
		</script>
	</head>
	<body>
		<%@ include file="/include/header.jsp" %>
		<div id="content" style="padding:0;">
			<div class="swiper-container">
				<div class="swiper-wrapper" style="position:relative;">
					<div id="tempDisplay">
						<%if(kcKegTemp.isValid()) {%>
						<%=kcKegTemp.getValueWithUnit()%>
						<%} else {%>
						&nbsp;
						<%}%>
					</div>
					<div class="swiper-slide">
						<div class="beer-slide">
							<div id="onTapHeader" class="left">Left Tap</div>
							<div id="onTapTitle" class="center"<%=onTapLeft == null ? " style=\"color:rgba(0, 0, 0, 0.2);\"" : ""%>><%=leftName%></div>
							<div id="onTapLocation" class="center"><%=leftLocation%></div>
							<div class="onTapImageContainer"><span class="helper"></span><img class="onTapImage" src="<%=leftImage%>"/></div>
							<div class="onTapDetails">
								<span><%=leftIbu%></span>
								<span><%=leftStyle%></span>
								<span<%=leftHighAbv ? " class=\"highAbv\"" : ""%>><%=leftAbv%></span>
							</div>
							<div class="onTapDescription"><%=leftDescription%></div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="beer-slide">
							<div id="onTapHeader" class="right">Right Tap</div>
							<div id="onTapTitle" class="center"<%=onTapRight == null ? " style=\"color:rgba(0, 0, 0, 0.2);\"" : ""%>><%=rightName%></div>
							<div id="onTapLocation" class="center"><%=rightLocation%></div>
							<div class="onTapImageContainer"><span class="helper"></span><img class="onTapImage" src="<%=rightImage%>"/></div>
							<div class="onTapDetails">
								<span><%=rightIbu%></span>
								<span><%=rightStyle%></span>
								<span<%=rightHighAbv ? " class=\"highAbv\"" : ""%>><%=rightAbv%></span>
							</div>
							<div class="onTapDescription"><%=rightDescription%></div>
						</div>
					</div>
				</div>
				
				<div class="swiper-pagination"></div>
			</div>
		</div>
		<div id="headerCenter">
		    Kansas City&nbsp;&nbsp;|&nbsp;&nbsp;<a href="dallas/">Dallas</a>
		</div>
		
		<script>
			var swiper = new Swiper('.swiper-container', {
				pagination: '.swiper-pagination',
				slidesPerView: 2,
				spaceBetween: 0,
				breakpoints: {
					1199: {
						slidesPerView: 1,
						spaceBetween: 0
					}
				}
			});
		</script>
	</body>
</html>
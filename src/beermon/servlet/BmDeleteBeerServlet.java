package beermon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;
import beermon.data.BmData.BmDataException;
import beermon.data.BmSessionData;

public class BmDeleteBeerServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmDeleteBeerServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		logger.debug("Processing delete beer request for beer " + id);
		
		if(id == null || id.trim().isEmpty()) {
			forward(req, resp, false, "Beer ID is required.");
			return;
		}
		
		try {
			String result = BmData.deleteBeer(id);
			forward(req, resp, true,  result + " deleted successfully.");
		} catch(BmDataException e) {
			forward(req, resp, false, e.getMessage());
		} catch(Exception e) {
			logger.error("Exception deleting beer!", e);
			resp.sendError(500);
		}
	}
	
	private void forward(HttpServletRequest request, HttpServletResponse response, boolean success, String message) throws ServletException, IOException {
		BmSessionData.putMessage(request, success, message);
		
		response.sendRedirect("/admin/managebeers.jsp");
	}
}

package beermon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;

public class BmTimestampServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmTimestampServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {			
			resp.setContentType("text/plain");
			resp.getWriter().print(String.valueOf(BmData.getBeerTapTimestamp()));
		} catch(Exception e) {
			logger.error("Exception getting taps timestamp", e);
			resp.sendError(500);
		}
	}
}

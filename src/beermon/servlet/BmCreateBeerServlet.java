package beermon.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.http.fileupload.util.Streams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;
import beermon.data.BmData.BmDataException;
import beermon.data.BmSessionData;
import beermon.object.BmBeer;

public class BmCreateBeerServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmCreateBeerServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(!ServletFileUpload.isMultipartContent(req)) {
			forward(req, resp, false, "Invalid form submission");
			return;
		}
		
		Map<String, String> fields = new HashMap<>();
		byte[] image = null;
		BmBeer beer = null;
		boolean editing = false;
		
		try {
			ServletFileUpload upload = new ServletFileUpload();
			FileItemIterator iter = upload.getItemIterator(req);
			
			while(iter.hasNext()) {
				FileItemStream item = iter.next();
				String name = item.getFieldName();
				InputStream stream = item.openStream();
				if(item.isFormField())
					fields.put(name, Streams.asString(stream));
				else if("image".equals(name))
					image = IOUtils.toByteArray(stream);
			}
			
			int id = -1;
			if(fields.containsKey("id")) {
				editing = true;
				id = Integer.parseInt(fields.get("id"));
			}
			
			beer = new BmBeer(id, fields.get("name"), image, fields.get("style"), fields.get("abv"), fields.get("ibu"), fields.get("location"), fields.get("description"));
			BmData.saveBeer(beer);
			
			forward(req, resp, true, beer.name + (editing ? " saved" : " created"));
		} catch(BmDataException e) {
			if(beer != null)
				BmSessionData.putBeer(req, beer);
			
			forward(req, resp, false, e.getMessage());
		} catch(Exception e) {
			logger.error("Exception processing beer creation request!", e);
			resp.sendError(500);
		}
	}
	
	private void forward(HttpServletRequest request, HttpServletResponse response, boolean success, String message) throws ServletException, IOException {
		BmSessionData.putMessage(request, success, message);
		
		if(success)
			response.sendRedirect("/admin/managebeers.jsp");
		else
			response.sendRedirect("/admin/createbeer.jsp");
	}
}

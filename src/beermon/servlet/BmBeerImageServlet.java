package beermon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;
import beermon.data.BmData.BmDataException;
import beermon.data.BmUpload;

public class BmBeerImageServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmBeerImageServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String uri = req.getRequestURI();
		if(!uri.contains("/beer/image/")) {
			logger.error("Received malformed beer request: " + uri);
			resp.sendError(400);
			return;
		}
		
		String idString = uri.substring(uri.indexOf("/beer/image/") + 12);
		int id = 0;
		try {
			id = Integer.parseInt(idString);
		} catch (NumberFormatException nfe) {
			logger.error("Received malformed beer request: " + uri);
			resp.sendError(400);
			return;
		}
		
		byte[] image = null;
		try {
			image = BmData.getBeerImage(id);
		} catch(BmDataException e) {
			logger.warn(e.getMessage());
			resp.sendError(404);
			return;
		} catch(Exception e) {
			logger.error("Exception retrieving beer image", e);
			resp.sendError(500);
			return;
		}
		
		if(image == null) {
			logger.warn("Beer " + id + " has no image. Using default instead!");
			try {
				image = IOUtils.toByteArray(getServletContext().getResourceAsStream("/images/beer_default.png"));
			} catch(Exception e) {
				logger.error("Exception retrieving default beer image", e);
				resp.sendError(500);
				return;
			}
		}
		
		if(BmUpload.isPng(image))
			resp.setContentType("image/png");
		else if(BmUpload.isJpg(image))
			resp.setContentType("image/jpeg");
		else if(BmUpload.isGif(image))
			resp.setContentType("image/gif");
		else {
			logger.error("Beer image in database was of unknown type!");
			resp.sendError(500);
			return;
		}
		
		resp.setContentLength(image.length);
		resp.getOutputStream().write(image);
	}
}

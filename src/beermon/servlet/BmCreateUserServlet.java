package beermon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;
import beermon.data.BmSessionData;

public class BmCreateUserServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmCreateBeerServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String user = req.getParameter("user");
		String pass = req.getParameter("pass");
		String confirm = req.getParameter("pass2");
		logger.debug("Processing create user request for user: " + user);
		
		if(user == null || user.trim().isEmpty() || pass == null || pass.trim().isEmpty()) {
			forward(req, resp, false, "User and password are both required.");
			return;
		}
		
		if(!pass.equals(confirm)) {
			forward(req, resp, false, "Passwords do not match.");
			return;
		}
		
		try {
			String result = BmData.createUser(user, pass);
			if(!result.equals(user)) {
				forward(req, resp, false, result);
				return;
			}
			
			forward(req, resp, true, "User " + user + " created successfully.");
			return;
		} catch(Exception e) {
			logger.error("Exception creating user!", e);
			resp.sendError(500);
		}
	}
	
	private void forward(HttpServletRequest request, HttpServletResponse response, boolean success, String message) throws ServletException, IOException {
		BmSessionData.putMessage(request, success, message);
		
		if(success)
			response.sendRedirect("/admin/manageusers.jsp");
		else
			response.sendRedirect("/admin/createuser.jsp");
	}
}

	package beermon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;
import beermon.data.BmSessionData;
import beermon.object.BmUser;

public class BmLoginServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmLoginServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = req.getParameter("user");
		String pass = req.getParameter("pass");
		String dest = req.getParameter("dest");
		logger.debug("Processing login request for user: " + username);
		
		try {
			BmUser user = BmData.login(username, pass);
			if(user != null) {
				//Login was successful. Put sessionId in session.
				logger.debug("Authentication succeeded.");
				req.getSession().setAttribute("BmSessionId", user.sessionId);
				req.getSession().setAttribute("BmUserName", user.name);

				resp.sendRedirect(dest == null || dest.trim().isEmpty() ? "/admin" : dest);
			} else {
				logger.debug("Authentication failed.");
				BmSessionData.putMessage(req, false, "Authentication failed");
				resp.sendRedirect("/login/auth.jsp" + (dest == null || dest.trim().isEmpty() ? "" : ("?dest=" + dest)));
			}
		} catch(Exception e) {
			logger.error("Exception processing login request!", e);
			resp.sendError(500);
		}
	}
}

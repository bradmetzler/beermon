package beermon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;
import beermon.data.BmData.BmDataException;
import beermon.data.BmSessionData;
import beermon.websocket.BmUpdatePusher;

public class BmUpdateTapsServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmUpdateTapsServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String kcLeftIdString = req.getParameter("kcLeftTapBeerId");
		String kcRightIdString = req.getParameter("kcRightTapBeerId");
		String dallasLeftIdString = req.getParameter("dallasLeftTapBeerId");
		String dallasRightIdString = req.getParameter("dallasRightTapBeerId");
		
		int kcLeftBeerId = -1;
		int kcRightBeerId = -1;
		int dallasLeftBeerId = -1;
		int dallasRightBeerId = -1;
		
		try {
			kcLeftBeerId = Integer.parseInt(kcLeftIdString);
			kcRightBeerId = Integer.parseInt(kcRightIdString);
			dallasLeftBeerId = Integer.parseInt(dallasLeftIdString);
			dallasRightBeerId = Integer.parseInt(dallasRightIdString);
		} catch(NumberFormatException nfe) {
			logger.error("Invalid beer ID");
			forward(req, resp, false, "Invalid beer ID");
			return;
		}
		
		try {
			long beforeUpdate = BmData.getBeerTapTimestamp();
			BmData.setBeerOnTap("KC Left Tap", kcLeftBeerId);
			BmData.setBeerOnTap("KC Right Tap", kcRightBeerId);
			BmData.setBeerOnTap("Dallas Left Tap", dallasLeftBeerId);
			BmData.setBeerOnTap("Dallas Right Tap", dallasRightBeerId);
			long afterUpdate = BmData.getBeerTapTimestamp();
			
			if(afterUpdate > beforeUpdate) {
				//At least one of the beers actually changed. Notify clients
				BmUpdatePusher.push();
			}
			
			forward(req, resp, true, "Taps updated");
		} catch(BmDataException e) {
			logger.error("Problem updating taps", e);
			forward(req, resp, false, e.getMessage());
		} catch(Exception e) {
			logger.error("Exception processing tap update request!", e);
			resp.sendError(500);
		}
	}
	
	private void forward(HttpServletRequest request, HttpServletResponse response, boolean success, String message) throws ServletException, IOException {
		BmSessionData.putMessage(request, success, message);
		response.sendRedirect("/admin/managetaps.jsp");
	}
}

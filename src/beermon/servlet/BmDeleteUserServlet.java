package beermon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;
import beermon.data.BmData.BmDataException;
import beermon.data.BmSessionData;

public class BmDeleteUserServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmDeleteUserServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		logger.debug("Processing delete user request for user " + id);
		
		if(id == null || id.trim().isEmpty()) {
			forward(req, resp, false, "User is required.");
			return;
		}
		
		try {
			String result = BmData.deleteUser(id);
			forward(req, resp, true, "User " + result + " deleted successfully.");
		} catch(BmDataException e) {
			logger.warn(e.getMessage());
			forward(req, resp, false, e.getMessage());
		} catch(Exception e) {
			logger.error("Exception deleting user!", e);
			resp.sendError(500);
		}
	}
	
	private void forward(HttpServletRequest request, HttpServletResponse response, boolean success, String message) throws ServletException, IOException {
		BmSessionData.putMessage(request, success, message);
		
		response.sendRedirect("/admin/manageusers.jsp");
	}
}

package beermon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;

public class BmLogoutServlet extends HttpServlet {
	static final Logger logger = LoggerFactory.getLogger(BmLogoutServlet.class);
	
	private static final long serialVersionUID = 1980072538253797804L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("Processing logout request for user.");
		
		try {
			BmData.logout((String)req.getSession().getAttribute("BmSessionId"));
			req.getSession().removeAttribute("BmSessionId");
			req.getSession().removeAttribute("BmUserName");
			resp.sendRedirect("/");
		} catch(Exception e) {
			logger.error("Exception processing login request!", e);
			resp.sendError(500);
		}
	}
}
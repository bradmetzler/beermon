package beermon.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;

public class BmContextListener implements ServletContextListener {
	static final Logger logger = LoggerFactory.getLogger(BmContextListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.debug("Beermon Shutdown");
		BmData.shutdown();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.debug("Beermon Startup");
		BmData.init();
	}
}

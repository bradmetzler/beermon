package beermon.job;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.stream.JsonReader;

import beermon.data.BmData;
import beermon.object.BmSensor;

public class UpdateKegTempJob implements Job {
	static final Logger logger = LoggerFactory.getLogger(UpdateKegTempJob.class);
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		logger.debug("Updating keg temperatures...");
		
		logger.debug("Calling keg temperature API for Kansas City...");
		
		Response kcTempResponse = null;
		try {
			kcTempResponse = makeRestCall("GET", "http://decent-destiny-704.appspot.com/laxservices/device_info.php?&deviceid=0001F70968BEF777&limit=1&timezone=2&metric=0", null);
		} catch(Exception e) {
			logger.error("Exception calling keg temperature API for Kansas City!", e);
		}
		
		if(kcTempResponse != null && kcTempResponse.httpCode >= 200 && kcTempResponse.httpCode < 300) {
			try {
				processKcTempResponse(kcTempResponse.body);
			} catch(Exception e) {
				logger.error("Exception processing keg temperature for Kansas City!", e);
			}
		} else {
			logger.error("Keg temperature API for Kansas City returned http code: " + kcTempResponse.httpCode);
			if(kcTempResponse.body != null)
				logger.error("Keg temperature API for Kansas City returned body: " + kcTempResponse.body);
		}
	}
	
	private void processKcTempResponse(String response) throws IOException {
		logger.debug("Processing keg temperature for Kansas City...");
		
		float kcKegTemp = 0f;
		String kcKegUnit = null;
		String kcKegTimestamp = null;
		
		JsonReader reader = null;
		try {
			reader = new JsonReader(new StringReader(response));
			reader.beginObject();
			String key = null;
			while(reader.hasNext()) {
				key = reader.nextName();
				
				if("device0".equals(key)) {
					reader.beginObject();
					
					while(reader.hasNext()) {
						key = reader.nextName();
						if("obs".equals(key)) {
							reader.beginArray();
							
							if(reader.hasNext()) {
								reader.beginObject();
								
								while(reader.hasNext()) {
									key = reader.nextName();
									
									if("probe_temp".equals(key)) {
										kcKegTemp = Float.parseFloat(reader.nextString());
										kcKegUnit = "°F";
									} else if("timestamp".equals(key)) {
										kcKegTimestamp = reader.nextString();
									} else {
										reader.skipValue();
									}
								}
								
								reader.endObject();
							}
							
							while(reader.hasNext()) {
								//Eat any extra obs data.
								reader.skipValue();
							}
							
							reader.endArray();
						} else {
							reader.skipValue();
						}
					}
					
					reader.endObject();
				} else {
					reader.skipValue();
				}
			}
		} finally {
			if(reader != null)
				reader.close();
		}
		
		if(kcKegUnit != null && kcKegTimestamp != null) {
			BmSensor kcKegTempSensor = new BmSensor();
			kcKegTempSensor.name = "KC Keg Temperature";
			kcKegTempSensor.value = String.valueOf(kcKegTemp);
			kcKegTempSensor.unit = kcKegUnit;
			kcKegTempSensor.lastupdated = LocalDateTime.parse(kcKegTimestamp, DateTimeFormatter.ofPattern("M/d/yyyy h:mm a"));
			
			logger.debug("Found Kansas City keg temperature data: " + kcKegTemp + kcKegUnit + " as of " + kcKegTempSensor.getFormattedLastUpdateTime());
			
			try {
				BmData.updateSensor(kcKegTempSensor);
			} catch(Exception e) {
				logger.error("Exception updating Kansas City keg temperature sensor data in database!", e);
			}
		}
	}
	
	private Response makeRestCall(String httpMethod, String url, String body) throws Exception {
		BufferedReader in = null;

		try {
			URL backend = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) backend.openConnection();

			conn.setRequestMethod(httpMethod);
			if (body != null) {
				conn.setDoOutput(true);
				conn.getOutputStream().write(body.getBytes(Charset.forName("UTF-8")));
			}

			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), Charset.forName("UTF-8")));

			int httpCode = conn.getResponseCode();

			StringBuilder result = new StringBuilder();
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				result.append(inputLine);
			}

			return new Response(httpCode, result.toString());
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException ioe) {
				}
			}
		}
	}
	
	private class Response {
		public int httpCode;
		public String body;
		
		public Response(int httpCode, String body) {
			this.httpCode = httpCode;
			this.body = body;
		}
	}
}

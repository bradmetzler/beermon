package beermon.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.object.BmBeer;
import beermon.object.BmSensor;
import beermon.object.BmUser;
import beermon.security.BmHash;
import beermon.websocket.BmUpdatePusher;

public class BmData {
	static final Logger logger = LoggerFactory.getLogger(BmData.class);
	
	private static final int currentDbVersion = 5;
	private static Connection db = null;
	
	public static void init() {
		getDB();
	}
	
	public static long getBeerTapTimestamp() throws Exception {
		//Returns the time a tap or beer was updated, whichever is more recent
		Connection db = getDB();
		
		long latestUpdate = Long.MIN_VALUE;
		
		ResultSet taps = db.prepareStatement("SELECT beerid, updated FROM ontap;").executeQuery();
		while(taps.next()) {
			int beerId = taps.getInt(1);
			long tapUpdate = taps.getLong(2);
			
			if(tapUpdate > latestUpdate)
				latestUpdate = tapUpdate;
			
			PreparedStatement beerQuery = db.prepareStatement("SELECT updated FROM beer WHERE id = ?;");
			beerQuery.setInt(1, beerId);
			ResultSet beer = beerQuery.executeQuery();
			if(beer.next()) {
				long beerUpdate = beer.getLong(1);
				if(beerUpdate > latestUpdate)
					latestUpdate = beerUpdate;
			}
		}
		
		return latestUpdate;
	}
	
	/**
	 * @return true for beer created, false for beer updated
	 * @throws Exception
	 */
	public static boolean saveBeer(BmBeer beer) throws Exception {
		if(beer.name == null || beer.name.trim().isEmpty())
			throw new BmDataException("Beer name is required");
		
		if(beer.image.length == 0)
			beer.image = null;
		
		if(beer.image != null && beer.image.length > BmUpload.maxSize)
			throw new BmDataException("Max file size is " + BmUpload.maxSize + " bytes");
		
		if(beer.image != null && !BmUpload.isPng(beer.image) && !BmUpload.isJpg(beer.image) && !BmUpload.isGif(beer.image)) {
			beer.image = null;
			throw new BmDataException("Image must be PNG, JPG, or GIF");
		}
		
		boolean create = true;
		beer.name = beer.name.trim();
		
		Connection db = getDB();
		
		if(beer.id > 0) {
			//User is attempting to edit a beer. Let's make sure it exists.
			PreparedStatement existingQuery = db.prepareStatement("SELECT name FROM beer WHERE id = ?;");
			existingQuery.setInt(1, beer.id);
			ResultSet existing = existingQuery.executeQuery();
			if(!existing.next()) {
				logger.warn("Attempting to edit a beer that doesn't exist: " + beer.id);
				throw new BmDataException("Can't edit a beer that doesn't exist");
			}

			create = false;
		}
		
		PreparedStatement existingQuery = db.prepareStatement("SELECT id FROM beer WHERE name = ?;");
		existingQuery.setString(1, beer.name);
		ResultSet existing = existingQuery.executeQuery();
		if(existing.next()) {
			if(create || existing.getInt(1) != beer.id)
				throw new BmDataException("A beer with this name already exists");
		}
		
		if(create) {
			//Create new beer
			PreparedStatement createQuery = db.prepareStatement("INSERT INTO beer(name, image, style, abv, ibu, location, description, updated) VALUES(?, ?, ?, ?, ?, ?, ?, ?);");
			createQuery.setString(1, beer.name);
			createQuery.setBytes(2, beer.image);
			createQuery.setString(3, beer.style);
			createQuery.setString(4, beer.abv);
			createQuery.setString(5, beer.ibu);
			createQuery.setString(6, beer.location);
			createQuery.setString(7, beer.description);
			createQuery.setLong(8, System.currentTimeMillis());
			createQuery.executeUpdate();
		} else {
			//Update existing beer with or without new image
			boolean hasNewImage = beer.image != null && beer.image.length > 0;
			
			PreparedStatement updateQuery = db.prepareStatement("UPDATE beer SET name = ?, style = ?, abv = ?, ibu = ?, location = ?, description = ?, updated = ?" + (hasNewImage ? ", image = ?" : "") + " WHERE id = ?;");
			updateQuery.setString(1, beer.name);
			updateQuery.setString(2, beer.style);
			updateQuery.setString(3, beer.abv);
			updateQuery.setString(4, beer.ibu);
			updateQuery.setString(5, beer.location);
			updateQuery.setString(6, beer.description);
			updateQuery.setLong(7, System.currentTimeMillis());
			if(hasNewImage)
				updateQuery.setBytes(8, beer.image);
			updateQuery.setInt(hasNewImage ? 9 : 8, beer.id);
			updateQuery.executeUpdate();
		}
		
		return create;
	}
	
	public static String deleteBeer(String beerid) throws Exception {
		if(beerid == null || beerid.trim().isEmpty())
			throw new BmDataException("Beer ID is required!");
		
		int id = -1;
		String name = null;
		try {
			id = Integer.parseInt(beerid);
		} catch(NumberFormatException nfe) {
			throw new BmDataException("Beer ID must be a number!");
		}
		
		Connection db = getDB();
		
		PreparedStatement existingQuery = db.prepareStatement("SELECT name FROM beer WHERE id = ?;");
		existingQuery.setInt(1, id);
		ResultSet existingBeer = existingQuery.executeQuery();
		if(existingBeer.next())
			name = existingBeer.getString(1);
		else
			throw new BmDataException("Beer does not exist");
		
		PreparedStatement onTapUpdateQuery = db.prepareStatement("UPDATE ontap SET beerid = ? WHERE beerid = ?;");
		onTapUpdateQuery.setInt(1, -1);
		onTapUpdateQuery.setInt(2, id);
		onTapUpdateQuery.executeUpdate();
		
		PreparedStatement deleteQuery = db.prepareStatement("DELETE FROM beer WHERE id = ?;");
		deleteQuery.setInt(1, id);
		int numUpdates = deleteQuery.executeUpdate();
		
		if(numUpdates == 1)
			return name;
		else
			throw new BmDataException("Unknown error deleting beer!");
	}
	
	/**
	 * Does NOT return the image data
	 */
	public static List<BmBeer> getBeers() throws Exception {
		List<BmBeer> beers = new ArrayList<>();
		
		Connection db = getDB();
		ResultSet result = db.prepareStatement("SELECT id, name, style, abv, ibu, location, description, typeof(image) FROM beer ORDER BY name ASC;").executeQuery();
		while(result.next()) {
			BmBeer beer = new BmBeer(result.getInt(1), result.getString(2), null, result.getString(3), result.getString(4), result.getString(5), result.getString(6), result.getString(7)); 
			beer.setHasImage(!"null".equalsIgnoreCase(result.getString(8)));
			beers.add(beer);
		}
		
		return beers;
	}
	
	/**
	 * Does NOT return the image data
	 */
	public static BmBeer getBeer(int beerId) throws Exception {
		Connection db = getDB();
		PreparedStatement beerQuery = db.prepareStatement("SELECT name, style, abv, ibu, location, description, typeof(image) FROM beer WHERE id = ?;");
		beerQuery.setInt(1, beerId);
		ResultSet result = beerQuery.executeQuery();
		if(!result.next())
			throw new BmDataException("Beer with id " + beerId + " does not exist");
		
		BmBeer beer = new BmBeer(beerId, result.getString(1), null, result.getString(2), result.getString(3), result.getString(4), result.getString(5), result.getString(6)); 
		beer.setHasImage(!"null".equalsIgnoreCase(result.getString(7)));
		return beer;
	}
	
	public static byte[] getBeerImage(int beerId) throws Exception {
		Connection db = getDB();
		PreparedStatement beerQuery = db.prepareStatement("SELECT image FROM beer WHERE id = ?;");
		beerQuery.setInt(1, beerId);
		ResultSet result = beerQuery.executeQuery();
		if(!result.next())
			throw new BmDataException("Beer with id " + beerId + " does not exist");
		
		return result.getBytes(1);
	}
	
	public static BmBeer getBeerOnTap(String tapName) throws Exception {
		int tapId = getTapId(tapName);
		
		Connection db = getDB();
		PreparedStatement onTapQuery = db.prepareStatement("SELECT beerid FROM ontap WHERE tapid = ?;");
		onTapQuery.setInt(1, tapId);
		ResultSet onTapResult = onTapQuery.executeQuery();
		if(!onTapResult.next())
			return null;
		
		int beerId = onTapResult.getInt(1);
		if(beerId == -1)
			return null;
		
		return getBeer(beerId);
	}
	
	public static void setBeerOnTap(String tapName, int beerId) throws Exception {
		if(tapName == null || tapName.trim().isEmpty())
			throw new BmDataException("Tap name is required");
		
		tapName = tapName.trim();
		int tapId = getTapId(tapName);
		
		BmBeer beer = null;
		
		if(beerId > -1)
			beer = getBeer(beerId);	//Call will fail if beer does not exist
		
		Connection db = getDB();
		PreparedStatement existingTapQuery = db.prepareStatement("SELECT beerid FROM ontap WHERE tapid = ?;");
		existingTapQuery.setInt(1, tapId);
		ResultSet existingTapResult = existingTapQuery.executeQuery();
		
		PreparedStatement onTapQuery = null;
		if(existingTapResult.next()) {
			int currentBeerId = existingTapResult.getInt(1);
			if(currentBeerId == beerId)
				return;	//This beer is already on this tap.
			onTapQuery = db.prepareStatement("UPDATE ontap SET beerid = ?, updated = ? WHERE tapid = ?;");
		}
		else
			onTapQuery = db.prepareStatement("INSERT INTO ontap(beerid, updated, tapid) VALUES(?, ?, ?);");
		
		logger.debug("Updating tap " + tapName + " to: " + (beerId > -1 ? beer.name : "(Empty)"));
		onTapQuery.setInt(1, beerId);
		onTapQuery.setLong(2, System.currentTimeMillis());
		onTapQuery.setInt(3, tapId);
		onTapQuery.executeUpdate();
	}
	
	public static int getTapId(String tapName) throws Exception {
		Connection db = getDB();
		PreparedStatement tapQuery = db.prepareStatement("SELECT id FROM tap WHERE name = ?;");
		tapQuery.setString(1, tapName);
		ResultSet tapResult = tapQuery.executeQuery();
		if(!tapResult.next())
			throw new BmDataException("Tap '" + tapName + "' does not exist!");
		
		return tapResult.getInt(1);
	}
	
	public static List<BmUser> getUsers() throws Exception {
		Connection db = getDB();
		
		List<BmUser> users = new ArrayList<>();
		ResultSet userRecords = db.prepareStatement("SELECT id, name FROM user;").executeQuery();
		while(userRecords.next())
			users.add(new BmUser(userRecords.getInt(1), userRecords.getString(2), null));
		
		return users;
	}
	
	public static BmUser login(String user, String pass) throws Exception {
		if(user == null || user.trim().isEmpty() || pass == null || pass.trim().isEmpty())
			return null;
		
		user = user.trim();
		pass = pass.trim();
		
		Connection db = getDB();
		
		PreparedStatement query = db.prepareStatement("SELECT name, id, salt, hash FROM user WHERE lower(name) = ?;");
		query.setString(1, user.toLowerCase());
		ResultSet userRecord = query.executeQuery();
		if(userRecord.next()) {
			String name = userRecord.getString(1);
			int id = userRecord.getInt(2);
			String salt = userRecord.getString(3);
			String hash = userRecord.getString(4);
			
			String hashAttempt = BmHash.hash(pass, salt);
			if(hashAttempt.equals(hash)) {
				//User entered correct password. Create a new session id
				String sessionId = BmHash.generateSalt();
				long timeout = System.currentTimeMillis() + (30 * 60 * 1000);	//30 minutes
				
				PreparedStatement update = db.prepareStatement("UPDATE user SET sessionid = ?, sessionexpire = ? WHERE id = ?;");
				update.setString(1, sessionId);
				update.setLong(2, timeout);
				update.setInt(3, id);
				int updatedRows = update.executeUpdate();
				
				if(updatedRows != 1) {
					logger.error("Problem logging user in. Updated " + updatedRows + " rows!");
					return null;
				}
				
				return new BmUser(id, name, sessionId);
			} else {
				return null;
			}
		}
			
		return null;
	}
	
	public static void logout(String sessionId) throws Exception {
		if(sessionId == null)
			return;
		
		Connection db = getDB();
		
		PreparedStatement update = db.prepareStatement("UPDATE user SET sessionid = NULL, sessionexpire = NULL WHERE sessionid = ?;");
		update.setString(1, sessionId);
		update.executeUpdate();
	}
	
	public static boolean isSessionValid(String sessionId) throws Exception {
		if(sessionId == null)
			return false;
		
		Connection db = getDB();
		
		PreparedStatement query = db.prepareStatement("SELECT name, sessionexpire FROM user WHERE sessionid = ?;");
		query.setString(1, sessionId);
		ResultSet session = query.executeQuery();
		if(session.next()) {
			long sessionTimeout = session.getLong(2);
			return System.currentTimeMillis() < sessionTimeout;
		}
		
		return false;
	}
	
	public static void keepAlive(String sessionId) throws Exception {
		if(sessionId == null)
			return;
		
		Connection db = getDB();
		PreparedStatement update = db.prepareStatement("UPDATE user SET sessionexpire = ? WHERE sessionid = ?;");
		update.setLong(1, System.currentTimeMillis() + (30 * 60 * 1000));
		update.setString(2, sessionId);
		update.executeUpdate();
	}
	
	public static String createUser(String user, String pass) throws Exception {
		if(user == null || user.trim().isEmpty() || pass == null || pass.trim().isEmpty())
			return "Username and password are both required!";
		
		user = user.trim();
		
		Connection db = getDB();
		
		PreparedStatement existingQuery = db.prepareStatement("SELECT * FROM user WHERE lower(name) = ?;");
		existingQuery.setString(1, user.toLowerCase());
		ResultSet existingUser = existingQuery.executeQuery();
		if(existingUser.next())
			return "User already exists";
		
		String salt = BmHash.generateSalt();
		String hash = BmHash.hash(pass, salt);
		PreparedStatement createQuery = db.prepareStatement("INSERT INTO user(name, salt, hash) VALUES(?, ?, ?);");
		createQuery.setString(1, user);
		createQuery.setString(2, salt);
		createQuery.setString(3, hash);
		int numUpdates = createQuery.executeUpdate();
		
		if(numUpdates == 1)
			return user;
		else
			return "Unknown error creating user!";
	}
	
	public static String deleteUser(String userid) throws Exception {
		if(userid == null || userid.trim().isEmpty())
			throw new BmDataException("User ID is required!");
		
		int id = -1;
		String name = null;
		try {
			id = Integer.parseInt(userid);
		} catch(NumberFormatException nfe) {
			throw new BmDataException("User ID must be a number!");
		}
		
		Connection db = getDB();
		
		PreparedStatement existingQuery = db.prepareStatement("SELECT name FROM user WHERE id = ?;");
		existingQuery.setInt(1, id);
		ResultSet existingUser = existingQuery.executeQuery();
		if(existingUser.next())
			name = existingUser.getString(1);
		else
			throw new BmDataException("User does not exist");
		
		if("Brad".equalsIgnoreCase(name))
			throw new BmDataException("Brad Metzler is immortal!");
		
		PreparedStatement createQuery = db.prepareStatement("DELETE FROM user WHERE id = ?;");
		createQuery.setInt(1, id);
		int numUpdates = createQuery.executeUpdate();
		
		if(numUpdates == 1)
			return name;
		else
			throw new BmDataException("Unknown error deleting user!");
	}
	
	public static BmSensor getSensor(String sensorName) throws Exception {
		Connection db = getDB();
		PreparedStatement sensorQuery = db.prepareStatement("SELECT id, value, unit, lastupdated, timezone FROM sensor WHERE name = ?;");
		sensorQuery.setString(1, sensorName);
		ResultSet result = sensorQuery.executeQuery();
		if(!result.next())
			throw new BmDataException("Sensor with name " + sensorName + " does not exist");
		
		long lastUpdatedMilli = result.getLong(4);
		String timezone = result.getString(5);
		LocalDateTime lastUpdated = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastUpdatedMilli), ZoneId.of(ZoneId.SHORT_IDS.get(timezone)));
		
		BmSensor sensor = new BmSensor(result.getInt(1), sensorName, result.getString(2), result.getString(3), lastUpdated, timezone);
		return sensor;
	}
	
	public static void updateSensor(BmSensor sensor) throws Exception {
		Connection db = getDB();
		
		String oldValue = null;
		
		PreparedStatement existingQuery = null;
		if(sensor.id > 0) {
			existingQuery = db.prepareStatement("SELECT name, value, timezone FROM sensor WHERE id = ?;");
			existingQuery.setInt(1, sensor.id);
			
			ResultSet existingSensor = existingQuery.executeQuery();
			if(existingSensor.next()) {
				sensor.name = existingSensor.getString(1);
				sensor.timezone = existingSensor.getString(3);
				
				oldValue = existingSensor.getString(2);
			} else {
				throw new BmDataException("Sensor with ID " + sensor.id + " does not exist");
			}
		} else {
			existingQuery = db.prepareStatement("SELECT id, value, timezone FROM sensor WHERE name = ?;");
			existingQuery.setString(1, sensor.name);
			
			ResultSet existingSensor = existingQuery.executeQuery();
			if(existingSensor.next()) {
				sensor.id = existingSensor.getInt(1);
				sensor.timezone = existingSensor.getString(3);
				
				oldValue = existingSensor.getString(2);
			} else {
				throw new BmDataException("Sensor with name " + sensor.name + " does not exist");
			}
		}
		
		//Convert LocalDateTime to millis since epoch for database
		long millis = sensor.lastupdated.atZone(ZoneId.of(ZoneId.SHORT_IDS.get(sensor.timezone))).toInstant().toEpochMilli();

		PreparedStatement updateQuery = db.prepareStatement("UPDATE sensor SET value = ?, unit = ?, lastupdated = ? WHERE id = ?");
		updateQuery.setString(1, sensor.value);
		updateQuery.setString(2, sensor.unit);
		updateQuery.setLong(3, millis);
		updateQuery.setInt(4, sensor.id);
		
		updateQuery.executeUpdate();
		
		if(sensor.value != null && !sensor.value.equals(oldValue)) {
			//The temperature actually changed. Notify clients
			BmUpdatePusher.push();
		}
	}
	
	private static Connection getDB() {
		if(db != null)
			return db;
		
		String dataDir = null;
		try {
			InitialContext initialContext = new InitialContext();
			Context environmentContext = (Context) initialContext.lookup("java:/comp/env");
			dataDir = (String) environmentContext.lookup("beermonDataDir");
			
			if(!dataDir.endsWith("/") && !dataDir.endsWith("\\")) {
				if(dataDir.contains("\\"))
					dataDir += "\\";
				else
					dataDir += "/";
			}
		} catch(Exception e) {
			logger.error("Exception looking up data dir", e);
			dataDir = "";
		}
		
		try {
			String dbLoc = dataDir + "beermon.db";
			logger.debug("Using database: " + dbLoc);
			
			Class.forName("org.sqlite.JDBC");
		    db = DriverManager.getConnection("jdbc:sqlite:" + dbLoc);
		    initDb(db);
		    return db;
		} catch(Exception e) {
			logger.error("ERROR! Can't open beermon database!");
			throw new RuntimeException(e);
		}
	}
	
	private static void initDb(Connection db) {
		try {
			int dbVersion = 0;
			
			ResultSet infoTableCount = db.prepareStatement("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='info';").executeQuery();
			if(infoTableCount.next() && infoTableCount.getInt(1) == 1) {
				//Info table exists. What DB version are we running?
				ResultSet x = db.prepareStatement("SELECT value FROM info WHERE name = 'DBVERSION';").executeQuery();
				if(x.next()) {
					dbVersion = x.getInt(1);
				}
			}
			
			if(dbVersion == currentDbVersion) {
				logger.debug("Database is up to date (" + dbVersion + ").");
				return;
			}
			
			if(dbVersion < 1) {
				//Initial stuff
				logger.debug("Initializing new beermon database.");
				db.prepareStatement("CREATE TABLE info(name TEXT PRIMARY KEY NOT NULL, value TEXT NOT NULL);").execute();
				db.prepareStatement("CREATE TABLE tap(id INTEGER PRIMARY KEY NOT NULL, name TEXT NOT NULL);").execute();
				db.prepareStatement("CREATE TABLE beer(id INTEGER PRIMARY KEY NOT NULL, name TEXT NOT NULL, image BLOB, style TEXT, abv TEXT, ibu TEXT, location TEXT, description TEXT);").execute();
				db.prepareStatement("CREATE TABLE ontap(tapid INTEGER NOT NULL, beerid INTEGER NOT NULL);").execute();
				db.prepareStatement("CREATE TABLE user(id INTEGER PRIMARY KEY NOT NULL, name TEXT UNIQUE NOT NULL, salt TEXT NOT NULL, hash TEXT NOT NULL, sessionid TEXT, sessionexpire INTEGER)").execute();
				
				db.prepareStatement("INSERT INTO tap(name) VALUES('Left Tap');").execute();
				db.prepareStatement("INSERT INTO tap(name) VALUES('Right Tap');").execute();
				db.prepareStatement("INSERT INTO info(name, value) VALUES('DBVERSION', 1);").execute();
				db.prepareStatement("INSERT INTO user(name, salt, hash) VALUES('Brad', 'fk6qdqfdkbt1k8rkeh27h2oi1o', '41d90309bcb87f10a995c66b5a6f2e49580c33c932ae3f876b0a712811d2b1a814e82e4a7fe741f413b9fda6fccfa99391df26d7ba0f7f2a1f4b90306b189be6');").execute();
			}
			if(dbVersion < 2) {
				//Add timestamps to beer and ontap for the ajax call
				logger.debug("Updating beermon database to version 2");
				long currentTime = System.currentTimeMillis();
				db.prepareStatement("ALTER TABLE beer ADD COLUMN updated INTEGER;").execute();
				db.prepareStatement("ALTER TABLE ontap ADD COLUMN updated INTEGER;").execute();
				
				PreparedStatement x = db.prepareStatement("UPDATE beer SET updated = ?;");
				x.setLong(1, currentTime);
				x.execute();
				
				x = db.prepareStatement("UPDATE ontap SET updated = ?;");
				x.setLong(1, currentTime);
				x.execute();
				
				db.prepareStatement("UPDATE info SET value = '2' WHERE name = 'DBVERSION';").execute();
			}
			if(dbVersion < 3) {
				//Add some default beers
				logger.debug("Updating beermon database to version 3");
				
				BmDefaultBeers.insertDefaultBeers(db);
				
				db.prepareStatement("UPDATE info SET value = '3' WHERE name = 'DBVERSION';").execute();
			}
			if(dbVersion < 4) {
				//Add taps for Dallas office
				logger.debug("Updating beermon database to version 4");
				
				db.prepareStatement("UPDATE tap SET name = 'KC Left Tap' WHERE name = 'Left Tap';").execute();
				db.prepareStatement("UPDATE tap SET name = 'KC Right Tap' WHERE name = 'Right Tap';").execute();
				db.prepareStatement("INSERT INTO tap(name) VALUES('Dallas Left Tap');").execute();
				db.prepareStatement("INSERT INTO tap(name) VALUES('Dallas Right Tap');").execute();
				
				db.prepareStatement("UPDATE info SET value = '4' WHERE name = 'DBVERSION';").execute();
			}
			if(dbVersion < 5) {
				//Add sensor data (initially for KC keg temperature data)
				logger.debug("Updating beermon database to version 5");
				
				db.prepareStatement("CREATE TABLE sensor(id INTEGER PRIMARY KEY NOT NULL, name TEXT UNIQUE NOT NULL, value TEXT, unit TEXT, lastupdated INTEGER, timezone TEXT)").execute();
				db.prepareStatement("INSERT INTO sensor(name, timezone) VALUES('KC Keg Temperature', 'CST')").execute();
				
				db.prepareStatement("UPDATE info SET value = '5' WHERE name = 'DBVERSION';").execute();
			}
		} catch(Exception e) {
			logger.error("Error initializing database.");
			throw new RuntimeException(e);
		}
	}
	
	public static void shutdown() {
		if(db != null) {
			logger.debug("Closing database connection.");
			try{db.close();}catch(Exception e){}
		}
	}
	
	public static class BmDataException extends Exception {
		private static final long serialVersionUID = -1514759653522145758L;

		public BmDataException(String message) {
			super(message);
		}
	}
}

package beermon.data;

import javax.servlet.http.HttpServletRequest;

import beermon.object.BmBeer;

public class BmSessionData {
	public static class Message {
		public boolean msgGood;
		public String msg;
		
		public Message(boolean msgGood, String msg) {
			this.msgGood = msgGood;
			this.msg = msg;
		}
	}
	
	public static void putMessage(HttpServletRequest request, boolean msgGood, String msg) {
		request.getSession().setAttribute("msg", new Message(msgGood, msg));
	}
	
	public static Message getMessage(HttpServletRequest request) {
		Message msg = (Message)request.getSession().getAttribute("msg");
		
		if(msg != null)
			request.getSession().removeAttribute("msg");
		
		return msg;
	}
	
	public static void putBeer(HttpServletRequest request, BmBeer beer) {
		request.getSession().setAttribute("beer", beer);
	}
	
	public static BmBeer getBeer(HttpServletRequest request) {
		BmBeer beer = (BmBeer)request.getSession().getAttribute("beer");
		
		if(beer != null)
			request.getSession().removeAttribute("beer");
		
		return beer;
	}
}

package beermon.data;

public class BmUpload {
	public static final int maxSize = 5000000;	//5MB
	
	public static boolean isPng(byte[] data) {
		if(data == null || data.length < 8)
			return false;
		
		//PNG magic bytes are
		//89 50 4E 47 0D 0A 1A 0A
		if((short)data[0] != -119)
			return false;
		if((short)data[1] != 80)
			return false;
		if((short)data[2] != 78)
			return false;
		if((short)data[3] != 71)
			return false;
		if((short)data[4] != 13)
			return false;
		if((short)data[5] != 10)
			return false;
		if((short)data[6] != 26)
			return false;
		if((short)data[7] != 10)
			return false;
		
		return true;
	}
	
	public static boolean isGif(byte[] data) {
		if(data == null || data.length < 6)
			return false;
		
		//GIF magic bytes are
		//47 49 46 38 37 61
		//or
		//47 49 46 38 39 61
		if((short)data[0] != 71)
			return false;
		if((short)data[1] != 73)
			return false;
		if((short)data[2] != 70)
			return false;
		if((short)data[3] != 56)
			return false;
		if((short)data[4] != 55 && (short)data[4] != 57)
			return false;
		if((short)data[5] != 97)
			return false;
		
		return true;
	}
	
	public static boolean isJpg(byte[] data) {
		if(data == null || data.length < 4)
			return false;
		
		//JPG magic bytes are
		//FF D8 FF DB
		//or
		//FF D8 FF E0
		//or
		//FF D8 FF E1
		//or
		//FF D8 FF E2
		//or
		//FF D8 FF E3
		//or
		//FF D8 FF E8
		if((short)data[0] != -1)
			return false;
		if((short)data[1] != -40)
			return false;
		if((short)data[2] != -1)
			return false;
		if((short)data[3] != -37 && (short)data[3] != -32 && (short)data[3] != -31 && (short)data[3] != -30 && (short)data[3] != -29 && (short)data[3] != -24)
			return false;
		
		return true;
	}
}

package beermon.data;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.commons.io.IOUtils;

public class BmDefaultBeers {
	public static void insertDefaultBeers(Connection db) throws Exception {
		long currentTime = System.currentTimeMillis();
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		
		PreparedStatement defaultBeerInsert = db.prepareStatement("INSERT INTO beer(name, image, style, abv, ibu, location, description, updated) VALUES(?, ?, ?, ?, ?, ?, ?, ?);");
		
		defaultBeerInsert.setString(1, "Boulevard Wheat");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevardwheat.png")));
		defaultBeerInsert.setString(3, "Wheat");
		defaultBeerInsert.setString(4, "4.4");
		defaultBeerInsert.setString(5, "14");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "Boulevard Unfiltered Wheat Beer is a lively, refreshing ale with a natural citrusy flavor and distinctive cloudy appearance. This easy drinking American-style wheat beer has become our most popular offering, and the best-selling craft beer in the Midwest.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "CinderBlock Northtown Native");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/cinderblocknorthtownnative.png")));
		defaultBeerInsert.setString(3, "Ale");
		defaultBeerInsert.setString(4, "5");
		defaultBeerInsert.setString(5, "20");
		defaultBeerInsert.setString(6, "North Kansas City, Missouri");
		defaultBeerInsert.setString(7, "Northtown Native is a light, refreshing, full bodied ale that is brewed and fermented as if it were a lager. Up front the taste is light and crisp with a malt backbone and American Sterling hops finish, to create a great session beer.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Mother's Oktoberfest");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/mothersoktoberfest.png")));
		defaultBeerInsert.setString(3, "Oktoberfest");
		defaultBeerInsert.setString(4, "5.5");
		defaultBeerInsert.setString(5, "20");
		defaultBeerInsert.setString(6, "Springfield, Missouri");
		defaultBeerInsert.setString(7, "Old School Oktoberfest is a traditional amber lager brewed to celebrate the fall harvest. Extended aging creates a medium bodied beer with a smooth malty flavor and just the faintest touch of hops. Pull up your lederhosen, raise a stein, Ein Prosit!");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard 80 Acre");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevard80acre.png")));
		defaultBeerInsert.setString(3, "Hoppy Wheat Ale");
		defaultBeerInsert.setString(4, "5.5");
		defaultBeerInsert.setString(5, "20");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "With roots in two of today's most popular brewing styles, 80-Acre Hoppy Wheat Beer is the result of careful cultivation by our brewers and cellarmen. Their efforts to craft a hybrid yielded a bumper crop of flavor; a delightfully distinctive ale with the aroma of an IPA and the refreshing taste of a wheat beer.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard Bob's '47");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevardbobs47.png")));
		defaultBeerInsert.setString(3, "Oktoberfest");
		defaultBeerInsert.setString(4, "5.8");
		defaultBeerInsert.setString(5, "27");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "Our fall seasonal beer, Bob’s ’47 Oktoberfest is a medium-bodied, dark amber brew with a malty flavor and well-balanced hop character. With this Munich-style lager we salute our friend Bob Werkowitch, Master Brewer and graduate of the U.S. Brewer’s Academy, 1947.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard Dark Truth");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevarddarktruth.png")));
		defaultBeerInsert.setString(3, "Imperial Stout");
		defaultBeerInsert.setString(4, "9.7");
		defaultBeerInsert.setString(5, "60");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "This is a full bodied beer whose general characteristics include a black, almost opaque appearance with a rich chocolaty and moderatly fruity aroma. The flavor is has an intense roasted malt character, exhibiting elements of dark fruit, coffee and sweet chocolate. The hop character has a moderate, balancing bitterness.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard Radler");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevardradler.png")));
		defaultBeerInsert.setString(3, "Radler");
		defaultBeerInsert.setString(4, "4.05");
		defaultBeerInsert.setString(5, "12");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "Boulevard Ginger Lemon Radler is a zesty, refreshing take on the tradition of mixing beer with soda or lemonade to create a light, thirst-quenching beverage ideal for warm weather. Radler (literally “cyclist,”) takes its name from active German sportsmen of a hundred years ago, but our version tastes just as good even if you do nothing more strenuous than lifting it to your lips.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard Saison-Brett");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevardsaisonbrett.png")));
		defaultBeerInsert.setString(3, "Saison");
		defaultBeerInsert.setString(4, "8.5");
		defaultBeerInsert.setString(5, "38");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "Saison-Brett, based on our very popular Tank 7, is assertively dry hopped, then bottle conditioned with various yeasts, including Brettanomyces, a wild strain that imparts a distinctive earthy quality. Though this farmhouse ale was given three months of bottle age prior to release, further cellaring will continue to enhance the “Brett” character, if that's what you're after.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard Sixth Glass");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevardsixthglass.png")));
		defaultBeerInsert.setString(3, "Belgian Dark Strong Ale");
		defaultBeerInsert.setString(4, "10.5");
		defaultBeerInsert.setString(5, "22");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "\"Do you know what dwells in a glass?\" asks Ole, in Hans Christian Andersen's The Watchman of the Tower. Better known for stories such as The Little Mermaid, Andersen wrote this short, cautionary tale for a somewhat older audience. Our quadrupel ale, also meant for the mature connoisseur, is a deep and mysterious libation, dark auburn and full-bodied, its sweetness deceptive. As Ole describes the glasses in turn, their contents become more ominous until, in the sixth glass...");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard Tank 7");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevardtank7.png")));
		defaultBeerInsert.setString(3, "Saison");
		defaultBeerInsert.setString(4, "8.5");
		defaultBeerInsert.setString(5, "38");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "Here at Boulevard we have fermenter number seven, the black sheep of our cellar family. When our brewers were experimenting with variations on a traditional Belgian-style farmhouse ale, the perfect combination of elements came together in that very vessel. You could call it fate, but they called it Tank 7, and so it is. Beginning with a big surge of fruity aromatics and grapefruit-hoppy notes, the flavor of this complex, straw-colored ale tapers off to a peppery, dry finish.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard Tell-Tale Tart");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevardtelltaletart.png")));
		defaultBeerInsert.setString(3, "Sour");
		defaultBeerInsert.setString(4, "6.2");
		defaultBeerInsert.setString(5, "10");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "Deriving its name from Edgar Allan Poe's classic tale of madness and murder, our newest Smokestack Series release takes a predictably lighthearted approach to the subject of sourness. The subtle acidity of Tell-Tale Tart is answered by a soft, biscuity malt character, making this a sour ale that suggests, rather than announcing its tartness. Beginning with a bracing sharpness at first sip, the ale mellows on the palate to a smooth and faintly lingering finish.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Boulevard The Calling");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/boulevardthecalling.png")));
		defaultBeerInsert.setString(3, "Imperial India Pale Ale");
		defaultBeerInsert.setString(4, "8.5");
		defaultBeerInsert.setString(5, "75");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "The Calling is an undeniable IPA we were driven to make. It's our tribute to like-minded dreamers, adventurous spirits, and glass half-full optimists. It's also our most heavily hopped beer ever, bursting forth with unmistakable tropical fruit and pine hop aromas and flavor supported with a slightly sweet malt character, tapering to a crisp, dry finish. Heed your call and enjoy.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "Breckenridge Mango Mosaic");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/breckenridgemangomosaic.png")));
		defaultBeerInsert.setString(3, "Pale Ale");
		defaultBeerInsert.setString(4, "5.5");
		defaultBeerInsert.setString(5, "29");
		defaultBeerInsert.setString(6, "Littleton, Colorado");
		defaultBeerInsert.setString(7, "Mosaic hops naturally give off aromas of guava and mango, lending a unique sweetness to the hop character of this pale ale. To build upon the tropical attributes of the hops, we've added a healthy dose of mango. Mango Mosaic Pale Ale is a well-balanced montage of fruit flavors, fresh aromas, and refreshingly crisp goodness.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "KC Bier Co. Dunkel");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/kcbiercodunkel.png")));
		defaultBeerInsert.setString(3, "Dunkel");
		defaultBeerInsert.setString(4, "5");
		defaultBeerInsert.setString(5, "18");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "\"Dunkel\" means dark, and our Dunkel copies the traditional brown Munich lager that was the most widely drank beer in Bavaria at the turn of the 20th Century. Our Dunkel is a medium-bodied beer with a slightly sweet, toasted bread crust malt flavor and smooth finish accentuated with just enough noble hop bitterness to balance the malt.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "KC Bier Co. Hefeweizen");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/kcbiercohefeweizen.png")));
		defaultBeerInsert.setString(3, "Hefeweizen");
		defaultBeerInsert.setString(4, "5");
		defaultBeerInsert.setString(5, "13");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "\"Hefe\" means yeast and \"Weizen\" means wheat. So, the name \"Hefeweizen\" describes an unfiltered wheat ale that still contains the yeast. KCBC’s version of the style is deep gold, with a spritzy effervescence, and rich bready malt back-ground. Hefeweizen’s smooth malt body,low bitterness and fruity esters make this beer truly refreshing.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
		
		defaultBeerInsert.setString(1, "KC Bier Co. Helles");
		defaultBeerInsert.setBytes(2, IOUtils.toByteArray(cl.getResourceAsStream("defaultbeers/kcbiercohelles.png")));
		defaultBeerInsert.setString(3, "Helles");
		defaultBeerInsert.setString(4, "5");
		defaultBeerInsert.setString(5, "23");
		defaultBeerInsert.setString(6, "Kansas City, Missouri");
		defaultBeerInsert.setString(7, "\"Helles\" means \"light or bright\", which refers to the beer’s pale color. KCBC’s Helles is a golden, Munich-style lager, which is the most popular style in Bavaria. Our Helles is simple, honest and delicious. Just like homemade bread, you can enjoy it every day with almost any meal and never grow tired of it.");
		defaultBeerInsert.setLong(8, currentTime);
		defaultBeerInsert.executeUpdate();
	}
}

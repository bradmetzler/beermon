package beermon.websocket;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint(value = "/updater")
public class BmUpdatePusher {
	private static final Logger logger = LoggerFactory.getLogger(BmUpdatePusher.class);
	private static final Set<Session> clients = new CopyOnWriteArraySet<>();
	
	private Session session;
	
	@OnOpen
	public void start(Session session) {
		this.session = session;
		clients.add(session);
		logger.debug("New websocket client connected. Total clients: " + clients.size());
	}
	
	@OnClose
	public void end() {
		clients.remove(this.session);
		logger.debug("Websocket client disconnected. Total clients: " + clients.size());
	}
	
	@OnMessage
	public void incoming(String message) {
		logger.debug("Received message from websocket client for some reason: " + message);
	}
	
	public static void push() {
		logger.debug("Sending update notification to " + clients.size() + " websocket clients.");
		
		Set<Session> staleClients = new HashSet<>();
		Iterator<Session> iter = clients.iterator();
		while(iter.hasNext()) {
			Session client = iter.next();
			try {
				if(client.isOpen()) {
					client.getBasicRemote().sendText("Ahoy");
				} else {
					staleClients.add(client);
				}
			} catch(IOException e) {
				logger.error("Failed sending update notification to websocket client!", e);
			}
		}
		
		if(!staleClients.isEmpty()) {
			logger.debug("Removing " + staleClients.size() + " stale websocket clients!");
			clients.removeAll(staleClients);
		}
	}
}

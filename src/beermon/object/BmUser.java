package beermon.object;

public class BmUser {
	public int id;
	public String name;
	public String sessionId;
	
	public BmUser(int id, String name, String sessionId) {
		this.id = id;
		this.name = name;
		this.sessionId = sessionId;
	}
}

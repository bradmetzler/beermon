package beermon.object;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class BmSensor {
	public int id;
	public String name;
	public String value;
	public String unit;
	public LocalDateTime lastupdated;
	public String timezone;
	
	public BmSensor() {}
	public BmSensor(int id, String name, String value, String unit, LocalDateTime lastupdated, String timezone) {
		this.id = id;
		this.name = name;
		this.value = value;
		this.unit = unit;
		this.lastupdated = lastupdated;
		this.timezone = timezone;
	}
	
	public float getValueAsFloat() {
		try {
			return Float.parseFloat(value);
		} catch(Exception e) {
			return 0f;
		}
	}
	
	public String getValueWithUnit() {
		return value + " " + unit;
	}
	
	public boolean hasData() {
		return value != null && !value.isEmpty();
	}
	
	public boolean isValid() {
		return isValid(ZoneId.of(ZoneId.SHORT_IDS.get(this.timezone)));
	}
	public boolean isValid(ZoneId timezone) {
		if(lastupdated == null)
			return false;
		
		LocalDateTime now = LocalDateTime.now(timezone);
		
		if(lastupdated.plusHours(6).isBefore(now)) {
			return false;
		}
		
		return true;
	}
	
	public String getFormattedLastUpdateTime() {
		if(lastupdated == null)
			return "";
		
		return lastupdated.format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm a")) + (this.timezone != null ? (" " + this.timezone) : "");
	}
}

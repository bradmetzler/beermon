package beermon.object;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BmBeer {
	private static final Logger logger = LoggerFactory.getLogger(BmBeer.class);
	private static final float highAbvWatermark = 7.5f;
	
	public int id;
	public String name;
	public byte[] image;
	public String style;
	public String abv;
	public String ibu;
	public String location;
	public String description;
	private boolean hasImage;
	
	public BmBeer(){}
	public BmBeer(int id, String name, byte[] image, String style, String abv, String ibu, String location, String description) {
		this.id = id;
		this.name = name;
		this.image = image;
		this.style = style;
		this.abv = abv;
		this.ibu = ibu;
		this.location = location;
		this.description = description;
	}
	
	public void setHasImage(boolean x) {
		this.hasImage = x;
	}
	
	public boolean hasImage() {
		return this.hasImage || (image != null && image.length > 0);
	}
	
	public boolean isHighAbv() {
		if(abv == null)
			return false;
		
		try {
			float abvFloat = Float.parseFloat(abv);
			return abvFloat > highAbvWatermark;
		} catch(NumberFormatException nfe) {
			logger.error("Beer '" + name + "' has unparseable abv: " + abv);
			return false;
 		}
	}
}

package beermon.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beermon.data.BmData;

public class BmBouncer implements Filter {
	static final Logger logger = LoggerFactory.getLogger(BmBouncer.class);
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain fc) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse resp = (HttpServletResponse)response;
		
		try {
			String sessionId = (String)req.getSession().getAttribute("BmSessionId");
			if(BmData.isSessionValid(sessionId)) {
				//User is logged in. Update their timeout so they are good for another 30 minutes.
				BmData.keepAlive(sessionId);
			} else {
				resp.sendRedirect("/login/auth.jsp?dest=" + req.getRequestURI());
				return;
			}
		} catch(Exception e) {
			logger.error("Bouncer might be drunk!", e);
		}
		
		fc.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig fc) throws ServletException {

	}
	
	@Override
	public void destroy() {}
}
